# Personal webpage

This is my personal webpage using [firebase](https://firebase.google.com/) as host. I put all my information up there. For more details, please go to [my webpage](https://lauchokyip.com)

## Building image
```bash
docker build  . -t lauchokyip/rpi-personal-website:2.0.6
```


## Update: Adding docker support
### Example of how to run with docker
To pull the docker image,
```bash
docker pull lauchokyip/personal-website-image
```
Then run the website by doing
```bash
docker run -p 8888:80  lauchokyip/personal-website-image:v1
```
Open the browser and run
```bash
localhost:8888
```

![my website](website.gif)