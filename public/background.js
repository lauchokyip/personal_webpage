
var c = document.getElementById("c");
var ctx = c.getContext("2d");


c.height = window.innerHeight;
c.width = window.innerWidth;

//making the canvas full screen
let old_height = c.height;
let old_width = c.width;

//chinese characters - taken from the unicode charset
var matrix = "0 0 1 1 0";
//converting the string into an array of single characters
matrix = matrix.split("");

var font_size = 10;
var columns = c.width / font_size; //number of columns for the rain
//an array of drops - one per column
var drops = [];
//x below is the x coordinate
//1 = y co-ordinate of the drop(same for every drop initially)
for (var i = 0; i < columns; i++)
  drops[i] = Math.floor(Math.random() * columns) ;


//drawing the characters
function draw() 
{

 
  //update the width if it has been changed
  if(old_width !=  window.innerWidth)
  {
    console.log("in")
    c.width = window.innerWidth;
    old_width = c.width;

    // this is to update the width of the drops
    columns = c.width / font_size; 
    for (var i = 0; i < columns; i++)
    {
      drops[i] = Math.floor(Math.random() * columns) ;
    }
 
  }

    //update the height if it has been changed
    if(old_height != window.innerHeight)
    {
      c.height = window.innerHeight;
      old_height = c.height;
    }


  //Black BG for the canvas
  //translucent BG to show trail
  ctx.fillStyle = "rgba(0, 0, 0, 0.1)";
  ctx.fillRect(0, 0, c.width, c.height);

  ctx.fillStyle = "#0F0"; //green text
  // //looping over drops
  for (var i = 0; i < drops.length; i++) 
  {
  //a random binary to print
    var text = matrix[Math.floor(Math.random() * matrix.length)];
    //x = i*font_size, y = value of drops[i]*font_size
    ctx.fillText(text, i * font_size, drops[i] * font_size);

    //incrementing Y coordinate
    drops[i]++;
  //sending the drop back to the top randomly after it has crossed the screen
  //adding a randomness to the reset to make the drops scattered on the Y axis
    if ( Math.random() > 0.925)
      drops[i] = Math.floor(Math.random() * columns);
      
  }
}
setInterval(draw, 50);

