$(
    function() {
        var anim = false;
        function typed(finish_typing) {
            return function(term, message, delay, finish) {
                anim = true;
                var prompt = term.get_prompt();
                var c = 0;
                if (message.length > 0) {
                    term.set_prompt('');
                    var new_prompt = '';
                    var interval = setInterval(function() {
                        var chr = $.terminal.substring(message, c, c+1);
                        new_prompt += chr;
                        term.set_prompt(new_prompt);
                        c++;
                        if (c == length(message)) {
                            clearInterval(interval);
                            // execute in next interval
                            setTimeout(function() {
                                // swap command with prompt
                                finish_typing(term, message, prompt);
                                anim = false
                                finish && finish();
                            }, delay);
                        }
                    }, delay);
                }
            };
    }
    function getRandomQuote() {
        // Template `, ""`,
        let arrQuote = [
            `Alan Perlis,  "A language that doesn't affect the way you think about programming, is not worth knowing"`,
            `Ken Thompson, "When in doubt, use brute force"`,
            `Ken Thompson, "You can't trust the code that you did not totally create yourself"`,
            `Rob Pike, "Simplicity is Complicated"`,
            `Dennis Ritchie, "UNIX is basically a simple operating system, but you have to be a genius to understand the simplicity"`,
            `Linus Torvalds, "Intelligence is the ability to avoid doing work, yet getting the work done."`,
            `Linus Torvalds, "Bad programmers worry about the code. Good programmers worry about data structures and their relationships."`,
            `Linus Torvalds, "Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program."`,
            `Peter Norvig, "One of the best programmers I ever hired had only a High School degree."`,
            `Martin Fowler, "Any fool can write code that a computer can understand. Good programmers write code that humans can understand."`,
            `John Kenneth Galbraith, "Faced With the Choice Between Changing One’s Mind and Proving That There Is No Need To Do So, Almost Everyone Gets Busy On the Proof"`,
            `Charles Munger, "The company that needs a new machine tool, and hasn't bought it, is already paying for it"`,
            `Charles Munger, "You have to figure out what your own aptitudes are. If you play games where other people have aptitudes and you don't, you are going to lose."`,
            `Baruch Spinoza, "All things excellent are as difficult as they are rare"`,
            `Benjamin Graham, "If he is right kind of investor he will take added satisfaction from the thought that his operations are exactly opposite from those of the crowd"`,
        ]
        randomNum = Math.floor((Math.random() * arrQuote.length ));
        return arrQuote[randomNum];
    }
    function getTodayDate() {
        let d = new Date();
        let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        return days[d.getDay()]
    }
    function length(string) {
        string = $.terminal.strip(string);
        return $('<span>' + string + '</span>').text().length;
    }
    var typed_prompt = typed(function(term, message, prompt) {
        term.set_prompt(message + ' ');
    });
    var typed_message = typed(function(term, message, prompt) {
        term.echo(message)
        term.set_prompt(prompt);
    });
    $('#terminal').terminal( 
        function(command, term)
        {
            if(command == 'help' || command == 'ls')
            {
                this.echo("Available commands:");
                this.echo("\twhoami      display my short brief");
                this.echo("\tcontact     display contact infomation");
                this.echo("\tresume      display my Latex Resume");
                this.echo("\tprojects    display things that I have worked on");
                this.echo("\thelp        this help screen.");                        
                this.echo("");      
            }
            else if(command == 'contact')
            {
                let div = $('<a href="mailto:lauchokyip@gmail.com" style="color:#0f0;">Email</a> <a href="https://www.linkedin.com/in/lauchokyip" style="color:#0f0;">LinkedIn</a> '  )
                this.echo(div);
            }
            else if(command == 'whoami')
            {
                let msg = "Hi! I am a software engineer and my dream is to be able to contribute to open source community full-time. Please let me know if you have such opportunity. Thank You :)\n";
                typed_message(term,msg,20, function(){
                    // typed_message(term,"Oh. Did I mention I am also a Linux User and I support FOSS!",50);
                });
            }
            else if(command == 'resume')
            {
                let div = $(' <a href="https://www.overleaf.com/read/qwwpxybpxpdg" style="color:#0f0;">Resume</a> '  )
                this.echo(div);
            }
            // else if(command == 'blog')
            // {
            //     var div = $(' <a href="blog.html" style="color:#0f0;">mYtLIfe</a> '  )
            //     this.echo(div);
            // }
            else if(command == 'projects')
            {
                this.echo("It's on the top left corner :)")
            }
            else
            {
                this.echo('bash: ' + command + ': command not found')
            }
       },
       {
        greetings: '',
        height: Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)/2.5,
        width: Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)/2,
        prompt: 'lau@localhost:[[;red;]~]$ ',
        onInit: function(term) 
            {
                term.echo("This website is powered by Rpi and K3s\n")
                var msg = getTodayDate() + ` Random Quote: \n` + getRandomQuote() + "\n";
                typed_message(term, msg, 25, function() {
                    term.echo("Available commands:");
                    term.echo("\twhoami      display my short brief");
                    term.echo("\tcontact     display contact infomation");
                    term.echo("\tresume      display my Latex Resume");
                    term.echo("\tprojects    display things that I have worked on");
                    term.echo("\thelp        display help screen");                        
                    term.echo("");     
                });
            },
        keydown: function(e) 
            {
                //disable keyboard when animating
                if (anim) {
                    return false;
                }
            }
    });
});
